# unwrap_all

[![crates.io](https://img.shields.io/crates/v/unwrap_all?logo=rust)](https://crates.io/crates/unwrap_all/)
[![docs.rs](https://docs.rs/unwrap_all/badge.svg)](https://docs.rs/unwrap_all)

With this crate I would like to explore the ergonomics of being able to unwrap multiple levels with one call.


## Example unwrap_all!(n_times, expression)

```rust
use unwrap_all::unwrap_all;

let nested: Option<Result<Option<Result<usize, ()>>, ()>> = Some(Ok(Some(Ok(42))));

let unpacked = unwrap_all!(4, nested);

assert_eq!(42, unpacked);
```

## Example expect_all!(n_times, message, expression)

Running this function will give you a panic with the message `1 - must fail: 23`, where the `1` tells you how many levels in the panic was caused.

```rust
use unwrap_all::expect_all;

fn must_fail() {
    let var = Some(Err::<usize, isize>(23));
    let _result: usize = expect_all!(2, "must fail", var);
}
```
