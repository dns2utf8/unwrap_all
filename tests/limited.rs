use unwrap_all::unwrap_all;

#[test]
fn zero() {
    let var = 42usize;
    let result: usize = unwrap_all!(0, var);

    //////////////////
    assert_eq!(42, result);
    println!("it_works!");
}

//*
#[test]
fn var() {
    let var = Some(Some(42usize));
    let result: usize = unwrap_all!(2, var);

    //////////////////
    assert_eq!(42, result);
    println!("it_works!");
}
// */
//*
#[test]
fn inline() {
    assert_eq!(42, unwrap_all!(2, Some(Some(42))));
    println!("it_works!");
}
// */
#[test]
#[should_panic(expected = "called `Result::unwrap()` on an `Err` value: 23")]
fn must_fail() {
    let var = Some(Err::<usize, isize>(23));
    let _result: usize = unwrap_all!(2, var);

    //////////////////
    unreachable!("this must not be reached");
}
