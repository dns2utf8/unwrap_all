use unwrap_all::expect_all;

#[test]
fn zero() {
    let var = 42usize;
    let result: usize = expect_all!(0, "this should be fine", var);

    //////////////////
    assert_eq!(42, result);
    println!("it_works!");
}

#[test]
fn var() {
    let var = Some(Ok::<usize, ()>(42));
    let result: usize = expect_all!(2, "all good", var);

    //////////////////
    assert_eq!(42, result);
    println!("it_works!");
}

#[test]
#[should_panic(expected = "1 - must fail: 23")]
fn must_fail() {
    let var = Some(Err::<usize, isize>(23));
    let _result: usize = expect_all!(2, "must fail", var);

    //////////////////
    unreachable!("this must not be reached");
}
