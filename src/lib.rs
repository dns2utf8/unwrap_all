//! Unpack multiple levels of `Result<T, E>` and `Option<T>` at once
//!
//! ```
//! use unwrap_all::unwrap_all;
//!
//! let nested: Option<Result<Option<Result<usize, ()>>, ()>> = Some(Ok(Some(Ok(42))));
//! let unpacked = unwrap_all!(4, nested);
//! assert_eq!(42, unpacked);
//! ```
//!
//! This crate should work with `no_std` too.

extern crate proc_macro;
use proc_macro::TokenStream as TokenStreamRustc;

use proc_macro2::{/*Delimiter, Ident, Literal, */ Spacing, /*Span, TokenStream,*/ TokenTree,};

/// Use it like this: `unwrap_all!(n_times, object)`
///
/// ```
/// use unwrap_all::unwrap_all;
///
/// let nested = Some(Ok::<usize, ()>(42));
/// let unpacked = unwrap_all!(2, nested);
///
/// assert_eq!(42, unpacked);
/// ```
#[proc_macro]
pub fn unwrap_all(input: TokenStreamRustc) -> TokenStreamRustc {
    let tokens = proc_macro2::TokenStream::from(input);
    let mut iter = tokens.clone().into_iter();

    let n_times = match iter.next().unwrap() {
        TokenTree::Literal(lit) => lit
            .to_string()
            .parse::<usize>()
            .expect("first argument must be unsigned {{integer}}"),
        _ => panic!("expected {{number}} - wrong token: {:?}", tokens),
    };

    match iter.next().unwrap() {
        TokenTree::Punct(punct) => {
            assert_eq!(punct.as_char(), ',');
            assert_eq!(punct.spacing(), Spacing::Alone);
        }
        _ => panic!("expected ',' - wrong token {:?}", tokens),
    }

    let unwraps = (0..n_times).flat_map(|_| {
        use quote::quote;

        quote! {
            .unwrap()
        }
    });

    let output: proc_macro2::TokenStream = iter.chain(unwraps).collect();

    proc_macro::TokenStream::from(output)
}

/// Running this function will give you a panic with the message `1 - must fail: 23`, where the `1` tells you how many levels in the panic was caused.
///
/// ```
/// use unwrap_all::expect_all;
///
/// fn must_fail() {
///     let var = Some(Err::<usize, isize>(23));
///     let _result: usize = expect_all!(2, "must fail", var);
/// }
/// ```
#[proc_macro]
pub fn expect_all(input: TokenStreamRustc) -> TokenStreamRustc {
    let tokens = proc_macro2::TokenStream::from(input);
    let mut iter = tokens.clone().into_iter();

    let n_times = match iter.next().unwrap() {
        TokenTree::Literal(lit) => lit
            .to_string()
            .parse::<usize>()
            .expect("first argument must be unsigned {{integer}}"),
        _ => panic!("expected {{number}} - wrong token: {:?}", tokens),
    };

    match iter.next().unwrap() {
        TokenTree::Punct(punct) => {
            assert_eq!(punct.as_char(), ',');
            assert_eq!(punct.spacing(), Spacing::Alone);
        }
        _ => panic!("expected ',' - wrong token {:?}", tokens),
    }

    // --------- message
    let message = match iter.next().unwrap() {
        TokenTree::Literal(lit) => {
            let s = lit.to_string();

            assert!(
                s.starts_with('"') && s.ends_with('"'),
                "message literal must be quoted"
            );

            s.trim_matches('"').to_owned()
            //.parse::<String>()
            //.expect("first argument must be {{String}}")
        }
        _ => panic!("expected {{String}} - wrong token: {:?}", tokens),
    };

    match iter.next().unwrap() {
        TokenTree::Punct(punct) => {
            assert_eq!(punct.as_char(), ',');
            assert_eq!(punct.spacing(), Spacing::Alone);
        }
        _ => panic!("expected ',' - wrong token {:?}", tokens),
    }

    let expects = (0..n_times).flat_map(move |level| {
        use quote::quote;

        let message = format!("{} - {}", level, message);

        quote! {
            .expect(#message)
        }
    });

    let output: proc_macro2::TokenStream = iter.chain(expects).collect();

    proc_macro::TokenStream::from(output)
}
