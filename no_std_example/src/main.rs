#![no_std]
#![no_main]

#[macro_use]
extern crate unwrap_all;
extern crate libc;

use core::panic::PanicInfo;

fn ok() {
    let nested = Some(Some(Ok::<usize, ()>(42)));

    let unpacked = unwrap_all!(3, nested);

    assert_eq!(42, unpacked);
}

fn nok() {
    let nested = Some(Some(Ok::<usize, ()>(42)));

    let _ = expect_all!(3, "this did not work", nested);

    unreachable!("must panic before");
}

#[no_mangle]
pub extern "C" fn main() {
    ok();
    nok();
}

//#[eh_personality]
//extern "C" fn eh_personality() {}

/*
#[no_mangle]
pub extern "C" fn _start() -> ! {
    main();
    loop {}
}
*/
#[no_mangle]
pub extern "C" fn __libc_csu_init() -> () {
    main();
}
#[no_mangle]
pub extern "C" fn __libc_csu_fini() -> ! {
    loop {}
}
#[no_mangle]
pub extern "C" fn __libc_start_main() -> () {
    main();
}

#[panic_handler]
pub fn panic_fmt(_info: &PanicInfo) -> ! {
    // unsafe { libc::_exit(23) }
    loop {}
}
